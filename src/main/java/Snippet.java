import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;

import javax.swing.JButton;
import javax.swing.JFrame;

import com.michaelbaranov.microba.calendar.DatePicker;

public class Snippet {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLayout(new FlowLayout());

		final DatePicker picker = new DatePicker();
		picker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(picker.getDate());
			}
		});

		JButton btn = new JButton("NULL");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					picker.setDate(null);
				} catch (PropertyVetoException e1) {
					System.out.println("Could not set null date.");
					e1.printStackTrace();
				}
				picker.setEnabled(false);
			}
		});
		frame.add(picker);
		frame.add(btn);
		frame.setSize(640, 480);
		frame.setVisible(true);
	}
}
